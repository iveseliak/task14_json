package com.json.ex_json.parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.json.ex_json.entity.Person;

import java.io.File;
import java.io.IOException;

public class JacksonDataBind {
    public static void parserJson(String path){
        ObjectMapper objectMapper=new ObjectMapper();

        Person person=null;
        try {
            person=objectMapper.readValue(new File(path),Person.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(person);

        System.out.println(person.getFirstName());
    }
}
